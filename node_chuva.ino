#include <Wire.h>
#include "SSD1306Wire.h"

#include "icons.h"

/*
 * Define your own appid.h with your app id from open weather map.
 * the file should contain this:
 * const char* app_id = "your app id";
 */
#include "appid.h"

/*
 * Define your own wifi_info.h with your wifi credentials.
 * the file should contain this:
 * const char* ssid = "your ssid";
 * const char* password = "your password";
 */
#include "wifi_info.h"

#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <WiFiClient.h>

#include <ArduinoJson.h>

typedef struct{
  float temp;
  const char* weather;
  const char* icon;
} climate_data;

//Pinos do NodeMCU
// SDA => D1
// SCL => D2

SSD1306Wire display(0x3c, D1, D2);

void setup()
{
  //Serial.begin(115200);
  display.init();
  display.flipScreenVertically();

  //Wifi
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    connectingScreen();
  }
}

void connectingScreen()
{
  //Clear the display
  display.clear();
  display.setTextAlignment(TEXT_ALIGN_CENTER);
  //Select font
  display.setFont(ArialMT_Plain_16);
  
  display.drawString(63, 10, "Connecting");
  display.display();
  delay(100);

  display.clear();
  display.drawString(63, 10, "Connecting.");
  display.display();
  delay(100);

  display.clear();
  display.drawString(63, 10, "Connecting..");
  display.display();
  delay(100);

  display.clear();
  display.drawString(63, 10, "Connecting...");
  display.display();
}

void getWeatherInfo(){
  //Serial.println("Get info");
  display.setTextAlignment(TEXT_ALIGN_LEFT);
  HTTPClient http;

  String url = "http://api.openweathermap.org/data/2.5/forecast?q=Sao+Paulo,br&lang=pt&cnt=3&units=metric&appid=" +
    String(app_id);
  
  if (http.begin(url)) {
    int httpCode = http.GET();
    //Serial.println("Get info response code: " + String(httpCode));
    
    if(httpCode == 200){
      String payload = http.getString();

      climate_data response[3];
      
      parseWeather(payload.c_str(), response);
      
      display.clear();
      for(int i=0; i<3; i++){
        //String weatherStr = String(response[i].temp)+"C"+response[i].weather;
        String weatherStr = String(response[i].temp)+"C";
        uint16_t width = display.getStringWidth(weatherStr);
        display.drawString(2,8 + (i*16), weatherStr);
        display.drawXbm(2 + width, 8 + (i*16), 16, 16, getIcon(response[i].icon));
        display.drawString(2 + width + 17,8 + (i*16), response[i].weather);
        //display.drawString(64, (i*15)+16, response[i].weather);
      }
      display.display();
    }else{
      display.clear();
      display.drawString(64, 2, "Error " + String(httpCode));
      display.display();
    }
  }
  http.end();
}

void ProgressBar()
{
  for (int counter = 0; counter <= 100; counter++)
  {
    display.clear();
    //Desenha a barra de progresso
    display.drawProgressBar(0, 32, 120, 10, counter);
    //Atualiza a porcentagem completa
    display.setTextAlignment(TEXT_ALIGN_CENTER);
    display.drawString(64, 15, String(counter) + "%");
    display.display();
    delay(10);
  }
}

void loop()
{
  getWeatherInfo();
  delay(60000);
  ProgressBar();
  delay(1000);
}
