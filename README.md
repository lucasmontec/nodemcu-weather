

Simple NodeMCU Weather reader with an OLED display
==================================================

# About

This is a simple node MCU starter project that I did. It connects to the openweathermap API
and downloads the forecast for the next 3, 6 and 9 hours. Then it reads the json and gets the
data that is relevant to me, the weather and temperature. Then it prints those in a nice way
with cute little 16x16 icons that I did.

![the picture shows a node mcu and an oled display connected. the display shows the forecast.](https://i.imgur.com/JtjuRDj.jpg)

# Connections

The connections are quite simple:

![scl is connected to d2 and sda is connected to d1](https://i.imgur.com/3QCzDTo.png)


# Libraries and links

The API that I used:
http://openweathermap.org/

For Json Parsing I used:
https://arduinojson.org/

And the OLED library is:
https://github.com/ThingPulse/esp8266-oled-ssd1306

To create the icons I used:
https://xbm.jazzychad.net/

To get which icon does what I used this:
https://openweathermap.org/weather-conditions