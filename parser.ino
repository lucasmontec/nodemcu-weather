void parseWeather(const char* json, climate_data *data){
  const size_t capacity = 
    3*JSON_ARRAY_SIZE(1) +
    JSON_ARRAY_SIZE(3) +
    9*JSON_OBJECT_SIZE(1) +
    4*JSON_OBJECT_SIZE(2) +
    3*JSON_OBJECT_SIZE(4) +
    JSON_OBJECT_SIZE(5) +
    4*JSON_OBJECT_SIZE(8) +
    3*JSON_OBJECT_SIZE(9) +
    840;
  
  DynamicJsonDocument doc(capacity);

  deserializeJson(doc, json);

  JsonArray list = doc["list"];

  for(int i=0; i<3; i++){
    JsonObject list_current = list[i];
    data[i].temp = list_current["main"]["temp"];
    data[i].weather = list_current["weather"][0]["main"];
    data[i].icon = list_current["weather"][0]["icon"];
  }
}

//https://openweathermap.org/weather-conditions
const unsigned char* getIcon(const char* iconid){
  //Rain
  if(strcmp(iconid,"10n") == 0 ||
     strcmp(iconid,"10d") == 0 ||
     strcmp(iconid,"09d") == 0 ||
     strcmp(iconid,"09n") == 0) 
  {
    return rain_16_16;
  }

  //Clouds
  if(strcmp(iconid,"03d") == 0 ||
     strcmp(iconid,"03n") == 0 ||
     strcmp(iconid,"04d") == 0 ||
     strcmp(iconid,"04n") == 0 )
  {
    return clouds_16_16;
  }

  //Thunder
  if(strcmp(iconid,"11d") == 0 ||
     strcmp(iconid,"11n") == 0)
  {
    return thunder_16_16;
  }
  
  return sunny_16_16;
}
